/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Cliente;
import Model.Main;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Eduarda
 */
public class CadastroClienteController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    TextField cpf, nome, endereco, telefone, dia, mes, ano;
    @FXML
    Label aviso;
    
    @FXML
    public void ajuda(){
        Main.trocaTela("/View/Ajuda.fxml");
    }
    @FXML
    public void alterarLocacao(){
        Main.trocaTela("/View/AlterarLocacao.fxml");
    }
    @FXML
    public void alterarVeiculo(){
        Main.trocaTela("/View/AlterarVeiculo.fxml");
    }
    @FXML
    public void cadastrarLocacao(){
        Main.trocaTela("/View/CadastrarLocacao.fxml");
    }
    @FXML
    public void cadastrarVeiculo(){
        Main.trocaTela("/View/CadastrarVeiculo.fxml");
    }
    @FXML
    public void efetuarPagamento(){
        Main.trocaTela("/View/EfetuarPagamento.fxml");
    }
    @FXML
    public void excluirLocacao(){
        Main.trocaTela("/View/ExcluirLocacao.fxml");
    }
    @FXML
    public void excluirVeiculo(){
        Main.trocaTela("/View/ExcluirVeiculo.fxml");
    }
    @FXML
    public void gerarOrcamento(){
        Main.trocaTela("/View/GerarOrcamento.fxml");
    }
    @FXML
    public void gerenciarTipos(){
        Main.trocaTela("/View/GerenciarTipos.fxml");
    }
    @FXML
    public void relatarLocacao(){
        Main.trocaTela("/View/RelatarLocacao.fxml");
    }
    @FXML
    public void relatarPagamento(){
        Main.trocaTela("/View/RelatarPagamento.fxml");
    }
    @FXML
    public void excluirMotorista(){
        Main.trocaTela("/View/ExcluirMotorista.fxml");
    }
    @FXML
    public void alterarMotorista(){
        Main.trocaTela("/View/AlterarMotorista.fxml");
    }
    @FXML
    public void voltar(){
        Main.trocaTela("/View/Menu.fxml");
    }
    @FXML
    public void cadastroCliente(){
        Main.trocaTela("/View/CadastroCliente.fxml");
    }
    @FXML
    public void alterarCliente(){
        Main.trocaTela("/View/AlterarClienteFXML.fxml");
    }
    @FXML
    public void excluirCliente(){
        Main.trocaTela("/View/ExcluirCliente.fxml");
    }
    @FXML
    public void cadastrarMotorista(){
        Main.trocaTela("/View/CadastrarMotorista.fxml");
    }
    @FXML
    public void cadastrar() throws ParseException{
        if(validar()){
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); 
            Date data = new Date(System.currentTimeMillis());
            java.sql.Date data1 = new java.sql.Date (data.getTime());
            java.sql.Date data2 = new java.sql.Date (formato.parse(formatar(dia, mes, ano)).getTime());
            Cliente c = new Cliente();
            c.setCpf(cpf.getText());
            c.setNome(nome.getText());
            c.setEndereco(endereco.getText());
            c.setTelefone(telefone.getText());
            c.setDataNasc(data2);
            c.setDataCadastro(data1);
            c.setStatusCliente(0);
            c.insert(); 
        }
 
    }
    
    public Boolean validar(){
        String z="Mensagem do Sistema: ";
        Calendar cal = GregorianCalendar.getInstance();
        Boolean x=true;
        if(!cpf.getText().matches("[0-9]+")||cpf.getText().length()!=11){
            z+="CPF inválido. ";
            x = false;
        }
        if(!telefone.getText().matches("[0-9]+")){
            z+="Telefone inválido. ";
            x = false;
        }
        if(!dia.getText().matches("[0-9]+")||Integer.parseInt(dia.getText())<=0||Integer.parseInt(dia.getText())>31){
            z+="Dia inválido. ";
            x = false;
        }
        if(!mes.getText().matches("[0-9]+")||Integer.parseInt(mes.getText())<=0||Integer.parseInt(mes.getText())>12){
            z+="Mês inválido. ";
            x = false;
        }
        if(!ano.getText().matches("[0-9]+")||Integer.parseInt(ano.getText())<=1899||Integer.parseInt(ano.getText())>(cal.get(Calendar.YEAR)-18)){
            z+="Ano inválido. ";
            x = false;
        }
        if(x == false){
            aviso.setText(z+"Por favor revise seu preenchimento.");
        }
        return x;
    }
    
    public String formatar(TextField dia, TextField mes, TextField ano){
        String junto;
        junto = dia.getText()+'/'+mes.getText()+'/'+ano.getText();
        System.out.println(junto);
        return junto;
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
