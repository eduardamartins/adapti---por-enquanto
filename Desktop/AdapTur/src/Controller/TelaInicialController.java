/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Main;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Eduarda
 */
public class TelaInicialController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    public void entrar(){
        Main.setEnderecoVolta("/View/Menu.fxml");
        Main.trocaTela("/View/Menu.fxml");  
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
