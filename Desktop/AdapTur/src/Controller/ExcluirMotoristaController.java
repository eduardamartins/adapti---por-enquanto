/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Cliente;
import Model.Main;
import Model.Motorista;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Eduarda
 */
public class ExcluirMotoristaController implements Initializable {
    Motorista motorista = null;
    @FXML
    private TableView<Motorista> tabelaMotoristaA;
    @FXML
    private TableColumn <?, ?> colunaNomeC, colunaCPFC, colunaVencimentoC, colunaNumC, colunaCadastroC, colunaStatusC, colunaTelefoneC, colunaCategoriaC;
    
    @FXML
    public void ajuda(){
        Main.trocaTela("/View/Ajuda.fxml");
    }
    @FXML
    public void alterarLocacao(){
        Main.trocaTela("/View/AlterarLocacao.fxml");
    }
    @FXML
    public void alterarVeiculo(){
        Main.trocaTela("/View/AlterarVeiculo.fxml");
    }
    @FXML
    public void cadastrarLocacao(){
        Main.trocaTela("/View/CadastrarLocacao.fxml");
    }
    @FXML
    public void cadastrarVeiculo(){
        Main.trocaTela("/View/CadastrarVeiculo.fxml");
    }
    @FXML
    public void efetuarPagamento(){
        Main.trocaTela("/View/EfetuarPagamento.fxml");
    }
    @FXML
    public void excluirLocacao(){
        Main.trocaTela("/View/ExcluirLocacao.fxml");
    }
    @FXML
    public void excluirVeiculo(){
        Main.trocaTela("/View/ExcluirVeiculo.fxml");
    }
    @FXML
    public void gerarOrcamento(){
        Main.trocaTela("/View/GerarOrcamento.fxml");
    }
    @FXML
    public void gerenciarTipos(){
        Main.trocaTela("/View/GerenciarTipos.fxml");
    }
    @FXML
    public void relatarLocacao(){
        Main.trocaTela("/View/RelatarLocacao.fxml");
    }
    @FXML
    public void relatarPagamento(){
        Main.trocaTela("/View/RelatarPagamento.fxml");
    }
    @FXML
    public void excluirMotorista(){
        Main.trocaTela("/View/ExcluirMotorista.fxml");
    }
    @FXML
    public void alterarMotorista(){
        Main.trocaTela("/View/AlterarMotorista.fxml");
    }
    @FXML
    public void voltar(){
        Main.trocaTela("/View/Menu.fxml");
    }
    @FXML
    public void cadastroCliente(){
        Main.trocaTela("/View/CadastroCliente.fxml");
    }
    @FXML
    public void alterarCliente(){
        Main.trocaTela("/View/AlterarClienteFXML.fxml");
    }
    @FXML
    public void excluirCliente(){
        Main.trocaTela("/View/ExcluirCliente.fxml");
    }
    @FXML
    public void cadastrarMotorista(){
        Main.trocaTela("/View/CadastrarMotorista.fxml");
    }
    @FXML
    private void select(MouseEvent event) {
        motorista = tabelaMotoristaA.getSelectionModel().getSelectedItem();
    }
    @FXML
    public void desativar(){
        motorista.updateStatus(1);
        tabelaMotoristaA.refresh();
    }
    @FXML
    public void ativar(){
        motorista.updateStatus(0);
        tabelaMotoristaA.refresh();
    }

    
    private void makeColumns(){
        colunaNomeC.setCellValueFactory(new PropertyValueFactory<>("nome"));
        colunaCPFC.setCellValueFactory(new PropertyValueFactory<>("cpfFormatado"));
        colunaVencimentoC.setCellValueFactory(new PropertyValueFactory<>("vencHabilitacao"));
        colunaNumC.setCellValueFactory(new PropertyValueFactory<>("numHabilitacao"));
        colunaCategoriaC.setCellValueFactory(new PropertyValueFactory<>("categoria"));
        colunaCadastroC.setCellValueFactory(new PropertyValueFactory<>("dataHabilitaco"));
        colunaStatusC.setCellValueFactory(new PropertyValueFactory<>("statusMotorista"));
        colunaTelefoneC.setCellValueFactory(new PropertyValueFactory<>("telefone"));
    }
    private void addItems() {
        tabelaMotoristaA.getItems().clear();
        for(Motorista m : Motorista.getAll()){
            tabelaMotoristaA.getItems().add(m);
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.makeColumns();
        this.addItems();
    }    
    
}
