/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Cliente;
import Model.Main;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Eduarda
 */
public class AlterarClienteFXMLController implements Initializable {

    Cliente cliente = null;
    
    @FXML
    private TableView<Cliente> tabelaClienteA;
    @FXML
    private TableColumn <?, ?> colunaNomeC, colunaCPFC, colunaNascimentoC, colunaEnderecoC, colunaCadastroC, colunaStatusC, colunaTelefoneC;
    @FXML
    TextField cpf, nome, endereco, telefone, dia, mes, ano, TFnome;
    @FXML
    Label aviso;
    @FXML
    public void exibirTodos(){
        addItems();
    }
    @FXML
    public void buscarNome(){
        tabelaClienteA.getItems().clear();
        for(Cliente cli : Cliente.getAllNome(TFnome.getText())){
            tabelaClienteA.getItems().add(cli);
        }
        tabelaClienteA.refresh();
    }
    @FXML
    public void buscarCaractere(){
        tabelaClienteA.getItems().clear();
        for(Cliente cli : Cliente.getAllCaractere(TFnome.getText())){
            tabelaClienteA.getItems().add(cli);
        }
        tabelaClienteA.refresh();
    }
    @FXML
    public void buscarCPF(){
        tabelaClienteA.getItems().clear();
        for(Cliente cli : Cliente.getAllCPF(TFnome.getText())){
            tabelaClienteA.getItems().add(cli);
        }
        tabelaClienteA.refresh();
    }
    @FXML
    public void ajuda(){
        Main.trocaTela("/View/Ajuda.fxml");
    }
    @FXML
    public void alterarLocacao(){
        Main.trocaTela("/View/AlterarLocacao.fxml");
    }
    @FXML
    public void alterarVeiculo(){
        Main.trocaTela("/View/AlterarVeiculo.fxml");
    }
    @FXML
    public void cadastrarLocacao(){
        Main.trocaTela("/View/CadastrarLocacao.fxml");
    }
    @FXML
    public void cadastrarVeiculo(){
        Main.trocaTela("/View/CadastrarVeiculo.fxml");
    }
    @FXML
    public void efetuarPagamento(){
        Main.trocaTela("/View/EfetuarPagamento.fxml");
    }
    @FXML
    public void excluirLocacao(){
        Main.trocaTela("/View/ExcluirLocacao.fxml");
    }
    @FXML
    public void excluirVeiculo(){
        Main.trocaTela("/View/ExcluirVeiculo.fxml");
    }
    @FXML
    public void gerarOrcamento(){
        Main.trocaTela("/View/GerarOrcamento.fxml");
    }
    @FXML
    public void gerenciarTipos(){
        Main.trocaTela("/View/GerenciarTipos.fxml");
    }
    @FXML
    public void relatarLocacao(){
        Main.trocaTela("/View/RelatarLocacao.fxml");
    }
    @FXML
    public void relatarPagamento(){
        Main.trocaTela("/View/RelatarPagamento.fxml");
    }
    @FXML
    public void excluirMotorista(){
        Main.trocaTela("/View/ExcluirMotorista.fxml");
    }
    @FXML
    public void alterarMotorista(){
        Main.trocaTela("/View/AlterarMotorista.fxml");
    }
    @FXML
    public void voltar(){
        Main.trocaTela("/View/Menu.fxml");
    }
    @FXML
    public void cadastroCliente(){
        Main.trocaTela("/View/CadastroCliente.fxml");
    }
    @FXML
    public void alterarCliente(){
        Main.trocaTela("/View/AlterarClienteFXML.fxml");
    }
    @FXML
    public void excluirCliente(){
        Main.trocaTela("/View/ExcluirCliente.fxml");
    }
    @FXML
    public void cadastrarMotorista(){
        Main.trocaTela("/View/CadastrarMotorista.fxml");
    }
    @FXML
    public void atualizar() throws ParseException{
        if(validar()){
            cliente.setCpfAntigo(cliente.getCpf());
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); 
            Date data = new Date(System.currentTimeMillis());
            java.sql.Date data1 = new java.sql.Date (data.getTime());
            java.sql.Date data2 = new java.sql.Date (formato.parse(formatar()).getTime());
            cliente.setCpf(cpf.getText());
            cliente.setNome(nome.getText());
            cliente.setEndereco(endereco.getText());
            cliente.setTelefone(telefone.getText());
            cliente.setDataNasc(data2);
            cliente.setDataCadastro(data1);
            cliente.setStatusCliente(0);
            cliente.update(); 
            tabelaClienteA.refresh();
        }
    }
    public Boolean validar(){
        String z="Mensagem do Sistema: ";
        Calendar cal = GregorianCalendar.getInstance();
        Boolean x=true;
        if(!cpf.getText().matches("[0-9]+")||cpf.getText().length()!=11){
            z+="CPF inválido. ";
            x = false;
        }
        if(!telefone.getText().matches("[0-9]+")){
            z+="Telefone inválido. ";
            x = false;
        }
        if(!dia.getText().matches("[0-9]+")||Integer.parseInt(dia.getText())<=0||Integer.parseInt(dia.getText())>31){
            z+="Dia inválido. ";
            x = false;
        }
        if(!mes.getText().matches("[0-9]+")||Integer.parseInt(mes.getText())<=0||Integer.parseInt(mes.getText())>12){
            z+="Mês inválido. ";
            x = false;
        }
        if(!ano.getText().matches("[0-9]+")||Integer.parseInt(ano.getText())<=1899||Integer.parseInt(ano.getText())>(cal.get(Calendar.YEAR)-18)){
            z+="Ano inválido. ";
            x = false;
        }
        if(x == false){
            aviso.setText(z+"Por favor revise seu preenchimento.");
        }
        return x;
    }
    public String formatar(){
        String junto;
        junto = dia.getText()+'/'+mes.getText()+'/'+ano.getText();
        System.out.println(junto);
        return junto;
    }
    @FXML
    private void select(MouseEvent event) {
        SimpleDateFormat a = new SimpleDateFormat("d"), b = new SimpleDateFormat("M"), c = new SimpleDateFormat("Y");
        cliente = tabelaClienteA.getSelectionModel().getSelectedItem();
        nome.setText(cliente.getNome());
        cpf.setText(cliente.getCpf());
        dia.setText(a.format(cliente.getDataNasc())+"");
        mes.setText(b.format(cliente.getDataNasc())+"");
        ano.setText(c.format(cliente.getDataNasc())+"");
        telefone.setText(cliente.getTelefone());
        endereco.setText(cliente.getEndereco());
    }
    private void makeColumns(){
        colunaNomeC.setCellValueFactory(new PropertyValueFactory<>("nome"));
        colunaCPFC.setCellValueFactory(new PropertyValueFactory<>("cpfFormatado"));
        colunaNascimentoC.setCellValueFactory(new PropertyValueFactory<>("dataNasc"));
        colunaEnderecoC.setCellValueFactory(new PropertyValueFactory<>("endereco"));
        colunaCadastroC.setCellValueFactory(new PropertyValueFactory<>("dataCadastro"));
        colunaStatusC.setCellValueFactory(new PropertyValueFactory<>("statusCliente"));
        colunaTelefoneC.setCellValueFactory(new PropertyValueFactory<>("telefone"));
    }
    private void addItems() {
        tabelaClienteA.getItems().clear();
        for(Cliente cli : Cliente.getAllAtivos()){
            tabelaClienteA.getItems().add(cli);
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.makeColumns();
        this.addItems();
    }    
    
}
