/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Main;
import Model.Motorista;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Eduarda
 */
public class AlterarMotoristaController implements Initializable {

    Motorista motorista = null;
    @FXML
    private TableView<Motorista> tabelaMotoristaA;
    @FXML
    private TableColumn <?, ?> colunaNomeC, colunaCPFC, colunaVencimentoC, colunaNumC, colunaCadastroC, colunaStatusC, colunaTelefoneC, colunaCategoriaC;
    @FXML
    TextField cpf, nome, numCNH, telefone, diav, mesv, anov, diac, mesc, anoc, categoria;
    @FXML
    Label aviso;
    @FXML
    public void ajuda(){
        Main.trocaTela("/View/Ajuda.fxml");
    }
    @FXML
    public void alterarLocacao(){
        Main.trocaTela("/View/AlterarLocacao.fxml");
    }
    @FXML
    public void alterarVeiculo(){
        Main.trocaTela("/View/AlterarVeiculo.fxml");
    }
    @FXML
    public void cadastrarLocacao(){
        Main.trocaTela("/View/CadastrarLocacao.fxml");
    }
    @FXML
    public void cadastrarVeiculo(){
        Main.trocaTela("/View/CadastrarVeiculo.fxml");
    }
    @FXML
    public void efetuarPagamento(){
        Main.trocaTela("/View/EfetuarPagamento.fxml");
    }
    @FXML
    public void excluirLocacao(){
        Main.trocaTela("/View/ExcluirLocacao.fxml");
    }
    @FXML
    public void excluirVeiculo(){
        Main.trocaTela("/View/ExcluirVeiculo.fxml");
    }
    @FXML
    public void gerarOrcamento(){
        Main.trocaTela("/View/GerarOrcamento.fxml");
    }
    @FXML
    public void gerenciarTipos(){
        Main.trocaTela("/View/GerenciarTipos.fxml");
    }
    @FXML
    public void relatarLocacao(){
        Main.trocaTela("/View/RelatarLocacao.fxml");
    }
    @FXML
    public void relatarPagamento(){
        Main.trocaTela("/View/RelatarPagamento.fxml");
    }
    @FXML
    public void excluirMotorista(){
        Main.trocaTela("/View/ExcluirMotorista.fxml");
    }
    @FXML
    public void alterarMotorista(){
        Main.trocaTela("/View/AlterarMotorista.fxml");
    }
    @FXML
    public void voltar(){
        Main.trocaTela("/View/Menu.fxml");
    }
    @FXML
    public void cadastroCliente(){
        Main.trocaTela("/View/CadastroCliente.fxml");
    }
    @FXML
    public void alterarCliente(){
        Main.trocaTela("/View/AlterarClienteFXML.fxml");
    }
    @FXML
    public void excluirCliente(){
        Main.trocaTela("/View/ExcluirCliente.fxml");
    }
    @FXML
    public void cadastrarMotorista(){
        Main.trocaTela("/View/CadastrarMotorista.fxml");
    }
    @FXML
    public void atualizar() throws ParseException{
        System.out.println("FUNFA");
        if(validar()){
            motorista.setCpfAntigo(motorista.getCpf());
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); 
            java.sql.Date data1 = new java.sql.Date (formato.parse(formatar(diav, mesv, anov)).getTime());
            java.sql.Date data2 = new java.sql.Date (formato.parse(formatar(diac, mesc, anoc)).getTime());
            motorista.setCpf(cpf.getText());
            motorista.setNome(nome.getText());
            motorista.setNumHabilitacao(numCNH.getText());
            motorista.setTelefone(telefone.getText());
            motorista.setDataHabilitaco(data2);
            motorista.setVencHabilitacao(data1);
            motorista.setStatusMotorista(0);
            motorista.setCategoria(categoria.getText());
            motorista.update(); 
            tabelaMotoristaA.refresh();
        }
    }
    public String formatar(TextField dia, TextField mes, TextField ano){
        String junto;
        junto = dia.getText()+'/'+mes.getText()+'/'+ano.getText();
        System.out.println(junto);
        return junto;
    }
    public Boolean validar(){
        String z="Mensagem do Sistema: ";
        Calendar cal = GregorianCalendar.getInstance();
        Boolean x=true;
        if(!numCNH.getText().matches("[0-9]+")||numCNH.getText().length()!=11){
           z+="Número de CNH inválido. ";
           x = false; 
        }
        if(!cpf.getText().matches("[0-9]+")||cpf.getText().length()!=11){
            z+="CPF inválido. ";
            x = false;
        }
        if(!telefone.getText().matches("[0-9]+")){
            z+="Telefone inválido. ";
            x = false;
        }
        if(!diav.getText().matches("[0-9]+")||Integer.parseInt(diav.getText())<=0||Integer.parseInt(diav.getText())>31){
            z+="Dia de vencimento inválido. ";
            x = false;
        }
        if(!mesv.getText().matches("[0-9]+")||Integer.parseInt(mesv.getText())<=0||Integer.parseInt(mesv.getText())>12){
            z+="Mês de vencimento inválido. ";
            x = false;
        }
        if(!anov.getText().matches("[0-9]+")||Integer.parseInt(anov.getText())<=1899){
            z+="Ano de vencimento inválido. ";
            x = false;
        }
        if(!diac.getText().matches("[0-9]+")||Integer.parseInt(diac.getText())<=0||Integer.parseInt(diac.getText())>31){
            z+="Dia de cadastro inválido. ";
            x = false;
        }
        if(!mesc.getText().matches("[0-9]+")||Integer.parseInt(mesc.getText())<=0||Integer.parseInt(mesc.getText())>12){
            z+="Mês de cadastro inválido. ";
            x = false;
        }
        if(!anoc.getText().matches("[0-9]+")||Integer.parseInt(anoc.getText())<=1899){
            z+="Ano de cadastro inválido. ";
            x = false;
        }
        if(x == false){
            aviso.setText(z+"Por favor revise seu preenchimento.");
        }
        return x;
    }
    private void makeColumns(){
        colunaNomeC.setCellValueFactory(new PropertyValueFactory<>("nome"));
        colunaCPFC.setCellValueFactory(new PropertyValueFactory<>("cpfFormatado"));
        colunaVencimentoC.setCellValueFactory(new PropertyValueFactory<>("vencHabilitacao"));
        colunaNumC.setCellValueFactory(new PropertyValueFactory<>("numHabilitacao"));
        colunaCategoriaC.setCellValueFactory(new PropertyValueFactory<>("categoria"));
        colunaCadastroC.setCellValueFactory(new PropertyValueFactory<>("dataHabilitaco"));
        colunaStatusC.setCellValueFactory(new PropertyValueFactory<>("statusMotorista"));
        colunaTelefoneC.setCellValueFactory(new PropertyValueFactory<>("telefone"));
    }
    private void addItems() {
        tabelaMotoristaA.getItems().clear();
        for(Motorista m : Motorista.getAllAtivos()){
            tabelaMotoristaA.getItems().add(m);
        }
    }
    @FXML
    private void select(MouseEvent event) {
        SimpleDateFormat a = new SimpleDateFormat("d"), b = new SimpleDateFormat("M"), c = new SimpleDateFormat("Y");
        motorista = tabelaMotoristaA.getSelectionModel().getSelectedItem();
        nome.setText(motorista.getNome());
        cpf.setText(motorista.getCpf());
        diav.setText(a.format(motorista.getVencHabilitacao())+"");
        mesv.setText(b.format(motorista.getVencHabilitacao())+"");
        anov.setText(c.format(motorista.getVencHabilitacao())+"");
        diac.setText(a.format(motorista.getDataHabilitaco())+"");
        mesc.setText(b.format(motorista.getDataHabilitaco())+"");
        anoc.setText(c.format(motorista.getDataHabilitaco())+"");
        telefone.setText(motorista.getTelefone());
        categoria.setText(motorista.getCategoria());
        numCNH.setText(motorista.getNumHabilitacao());
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        makeColumns();
        addItems();
    }    
    
}
