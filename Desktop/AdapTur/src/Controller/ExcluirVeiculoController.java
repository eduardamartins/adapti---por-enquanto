/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Main;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Eduarda
 */
public class ExcluirVeiculoController implements Initializable {

    @FXML
    public void ajuda(){
        Main.trocaTela("/View/Ajuda.fxml");
    }
    @FXML
    public void alterarLocacao(){
        Main.trocaTela("/View/AlterarLocacao.fxml");
    }
    @FXML
    public void alterarVeiculo(){
        Main.trocaTela("/View/AlterarVeiculo.fxml");
    }
    @FXML
    public void cadastrarLocacao(){
        Main.trocaTela("/View/CadastrarLocacao.fxml");
    }
    @FXML
    public void cadastrarVeiculo(){
        Main.trocaTela("/View/CadastrarVeiculo.fxml");
    }
    @FXML
    public void efetuarPagamento(){
        Main.trocaTela("/View/EfetuarPagamento.fxml");
    }
    @FXML
    public void excluirLocacao(){
        Main.trocaTela("/View/ExcluirLocacao.fxml");
    }
    @FXML
    public void excluirVeiculo(){
        Main.trocaTela("/View/ExcluirVeiculo.fxml");
    }
    @FXML
    public void gerarOrcamento(){
        Main.trocaTela("/View/GerarOrcamento.fxml");
    }
    @FXML
    public void gerenciarTipos(){
        Main.trocaTela("/View/GerenciarTipos.fxml");
    }
    @FXML
    public void relatarLocacao(){
        Main.trocaTela("/View/RelatarLocacao.fxml");
    }
    @FXML
    public void relatarPagamento(){
        Main.trocaTela("/View/RelatarPagamento.fxml");
    }
    @FXML
    public void excluirMotorista(){
        Main.trocaTela("/View/ExcluirMotorista.fxml");
    }
    @FXML
    public void alterarMotorista(){
        Main.trocaTela("/View/AlterarMotorista.fxml");
    }
    @FXML
    public void voltar(){
        Main.trocaTela("/View/Menu.fxml");
    }
    @FXML
    public void cadastroCliente(){
        Main.trocaTela("/View/CadastroCliente.fxml");
    }
    @FXML
    public void alterarCliente(){
        Main.trocaTela("/View/AlterarClienteFXML.fxml");
    }
    @FXML
    public void excluirCliente(){
        Main.trocaTela("/View/ExcluirCliente.fxml");
    }
    @FXML
    public void cadastrarMotorista(){
        Main.trocaTela("/View/CadastrarMotorista.fxml");
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
