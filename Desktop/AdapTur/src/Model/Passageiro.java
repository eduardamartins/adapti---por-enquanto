/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Eduarda
 */
public class Passageiro {
    int tempoBase, telefonePassageiro, statusPassageiro;
    String enderecoPassageiro, nomePassageiro, cpfPassageiro;

    public int getTempoBase() {
        return tempoBase;
    }

    public void setTempoBase(int tempoBase) {
        this.tempoBase = tempoBase;
    }

    public int getTelefonePassageiro() {
        return telefonePassageiro;
    }

    public void setTelefonePassageiro(int telefonePassageiro) {
        this.telefonePassageiro = telefonePassageiro;
    }

    public int getStatusPassageiro() {
        return statusPassageiro;
    }

    public void setStatusPassageiro(int statusPassageiro) {
        this.statusPassageiro = statusPassageiro;
    }

    public String getEnderecoPassageiro() {
        return enderecoPassageiro;
    }

    public void setEnderecoPassageiro(String enderecoPassageiro) {
        this.enderecoPassageiro = enderecoPassageiro;
    }

    public String getNomePassageiro() {
        return nomePassageiro;
    }

    public void setNomePassageiro(String nomePassageiro) {
        this.nomePassageiro = nomePassageiro;
    }

    public String getCpfPassageiro() {
        return cpfPassageiro;
    }

    public void setCpfPassageiro(String cpfPassageiro) {
        this.cpfPassageiro = cpfPassageiro;
    }
    
    
}
