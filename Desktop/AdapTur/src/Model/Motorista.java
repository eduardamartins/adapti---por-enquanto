/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Eduarda
 *
 */
public class Motorista {
    Date dataHabilitaco, vencHabilitacao;
    String nome, categoria, cpf, numHabilitacao, telefone, cpfAntigo;
    int statusMotorista;

    public void setCpfAntigo(String cpfAntigo) {
        this.cpfAntigo = cpfAntigo;
    }

    public String getCpfAntigo() {
        return cpfAntigo;
    }

    public Date getDataHabilitaco() {
        return dataHabilitaco;
    }

    public void setDataHabilitaco(Date dataHabilitaco) {
        this.dataHabilitaco = dataHabilitaco;
    }

    public Date getVencHabilitacao() {
        return vencHabilitacao;
    }

    public String getCpfFormatado(){
        String x = "";
        int a;
        for(a=0; a<11;a++){
            if(a==3||a==6){
                x+= '.';
            }
            if(a==9){
                x+= '-';
            }
            x+= cpf.charAt(a);
        }
        return x;
    }

    public void setVencHabilitacao(Date vencHabilitacao) {
        this.vencHabilitacao = vencHabilitacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNumHabilitacao() {
        return numHabilitacao;
    }

    public void setNumHabilitacao(String numHabilitacao) {
        this.numHabilitacao = numHabilitacao;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getStatusMotorista() {
        return statusMotorista;
    }

    public void setStatusMotorista(int statusMotorista) {
        this.statusMotorista = statusMotorista;
    }
   //----------------------------------CRUD
    public boolean insert() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement;
        String insertTableSQL = "INSERT INTO motorista(cpfmotorista, nome, dataHabilitacao, numHabilitacao, telefone, vencHabilitacao, statusMotorista, categoria) VALUES(?,?,?,?,?,?,?,?)";
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, this.cpf);
            preparedStatement.setString(2, this.nome);
            preparedStatement.setDate(3, (java.sql.Date) this.dataHabilitaco);
            preparedStatement.setString(4, this.numHabilitacao);
            preparedStatement.setString(5, this.telefone);
            preparedStatement.setDate(6, (java.sql.Date) this.vencHabilitacao);
            preparedStatement.setInt(7, this.statusMotorista);
            preparedStatement.setString(8, this.categoria);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            c.desconecta();
        }
        return true;
    }
    public void updateStatus(int status) {
        setStatusMotorista(status);
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement;
        String sql = "update motorista set statusmotorista = ? where cpfmotorista = ?";
 
        try  {
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setInt(1, getStatusMotorista());
            preparedStatement.setString(2, getCpf());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            c.desconecta();
        }
    }
    public static ArrayList<Motorista> getAllAtivos() {
        String selectSQL = "select * from motorista where statusMotorista = 0";
        ArrayList<Motorista> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                Motorista m = new Motorista();  
                m.setCpf(rs.getString("cpfMotorista"));
                m.setNome(rs.getString("nome"));
                m.setDataHabilitaco(rs.getDate("dataHabilitacao"));
                m.setNumHabilitacao(rs.getString("numHabilitacao"));
                m.setTelefone(rs.getString("telefone"));
                m.setVencHabilitacao(rs.getDate("vencHabilitacao"));
                m.setStatusMotorista(rs.getInt("statusMotorista"));
                m.setCategoria(rs.getString("categoria"));
                lista.add(m);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
        return lista;
    }
    public void update() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement;
        String sql = "update motorista set cpfmotorista = ?, nome = ?, dataHabilitacao = ?, numHabilitacao = ?, telefone = ?, vencHabilitacao = ?, categoria = ? where cpfmotorista = ?";
        try  {
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setString(1, this.cpf);
            preparedStatement.setString(2, this.nome);
            preparedStatement.setDate(3, (java.sql.Date) this.dataHabilitaco);
            preparedStatement.setString(4, this.numHabilitacao);
            preparedStatement.setString(5, this.telefone);
            preparedStatement.setDate(6, (java.sql.Date) this.vencHabilitacao);
            preparedStatement.setString(7, this.categoria);
            preparedStatement.setString(8, getCpfAntigo());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            c.desconecta();
        }
    }
    public static ArrayList<Motorista> getAll() {
        String selectSQL = "select * from motorista";
        ArrayList<Motorista> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                Motorista m = new Motorista();  
                m.setCpf(rs.getString("cpfMotorista"));
                m.setNome(rs.getString("nome"));
                m.setDataHabilitaco(rs.getDate("dataHabilitacao"));
                m.setNumHabilitacao(rs.getString("numHabilitacao"));
                m.setTelefone(rs.getString("telefone"));
                m.setVencHabilitacao(rs.getDate("vencHabilitacao"));
                m.setStatusMotorista(rs.getInt("statusMotorista"));
                m.setCategoria(rs.getString("categoria"));
                lista.add(m);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
        return lista;
    }
}















































































































































































































































































































