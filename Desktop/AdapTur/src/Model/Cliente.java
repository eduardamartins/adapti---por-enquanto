/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;


/**
 *
 * @author Eduarda
 */
public class Cliente {
    Date dataCadastro, dataNasc;
    String nome, endereco, cpf, telefone, cpfAntigo;
    int statusCliente;

    public String getCpfAntigo() {
        return cpfAntigo;
    }

    public void setCpfAntigo(String cpfAntigo) {
        this.cpfAntigo = cpfAntigo;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(Date dataNasc) {
        this.dataNasc = dataNasc;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getStatusCliente() {
        return statusCliente;
    }

    public void setStatusCliente(int statusCliente) {
        this.statusCliente = statusCliente;
    }
    
    public String getCpfFormatado(){
        String x = "";
        int a;
        for(a=0; a<11;a++){
            if(a==3||a==6){
                x+= '.';
            }
            if(a==9){
                x+= '-';
            }
            x+= cpf.charAt(a);
        }
        return x;
    }
    
    //--------------------------------------CRUD
    public boolean insert() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "INSERT INTO cliente(cpfcliente, nome, datanasc, endereco, telefone, datacadastro, statuscliente) VALUES(?,?,?,?,?,?,?)";
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, this.cpf);
            preparedStatement.setString(2, this.nome);
            preparedStatement.setDate(3, (java.sql.Date) this.dataNasc);
            preparedStatement.setString(4, this.endereco);
            preparedStatement.setString(5, this.telefone);
            preparedStatement.setDate(6, (java.sql.Date) this.dataCadastro);
            preparedStatement.setInt(7, this.statusCliente);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            c.desconecta();
        }
        return true;
    }
    public void updateStatus(int status) {
        setStatusCliente(status);
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String sql = "update cliente set statuscliente = ? where cpfcliente = ?";
 
        try  {
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setInt(1, getStatusCliente());
            preparedStatement.setString(2, getCpf());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            c.desconecta();
        }
    }
    public static ArrayList<Cliente> getAllAtivos() {
        String selectSQL = "select * from cliente where statusCliente = 0";
        ArrayList<Cliente> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                Cliente cli = new Cliente();  
                cli.setCpf(rs.getString("cpfcliente"));
                cli.setNome(rs.getString("nome"));
                cli.setDataNasc(rs.getDate("dataNasc"));
                cli.setEndereco(rs.getString("endereco"));
                cli.setTelefone(rs.getString("telefone"));
                cli.setDataCadastro(rs.getDate("dataCadastro"));
                cli.setStatusCliente(rs.getInt("statusCliente"));
                lista.add(cli);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
        return lista;
    }

    public void update() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String sql = "update cliente set cpfcliente = ?, nome = ?, datanasc = ?, endereco = ?, telefone = ? where cpfcliente = ?";
        try  {
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setString(1, getCpf());
            preparedStatement.setString(2, getNome());
            preparedStatement.setDate(3, (java.sql.Date) getDataNasc());
            preparedStatement.setString(4, getEndereco());
            preparedStatement.setString(5, getTelefone());
            preparedStatement.setString(6, getCpfAntigo());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            c.desconecta();
        }
    }
    
    public static ArrayList<Cliente> getAll() {
        String selectSQL = "select * from cliente";
        ArrayList<Cliente> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                Cliente cli = new Cliente();  
                cli.setCpf(rs.getString("cpfcliente"));
                cli.setNome(rs.getString("nome"));
                cli.setDataNasc(rs.getDate("dataNasc"));
                cli.setEndereco(rs.getString("endereco"));
                cli.setTelefone(rs.getString("telefone"));
                cli.setDataCadastro(rs.getDate("dataCadastro"));
                cli.setStatusCliente(rs.getInt("statusCliente"));
                lista.add(cli);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
        return lista;
    }
    public static ArrayList<Cliente> getAllNome(String string) {
        String selectSQL = "select * from cliente where nome LIKE ?";
        ArrayList<Cliente> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            PreparedStatement ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, string+"%");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Cliente cli = new Cliente(); 
                cli.setCpf(rs.getString("cpfcliente"));
                cli.setNome(rs.getString("nome"));
                cli.setDataNasc(rs.getDate("dataNasc"));
                cli.setEndereco(rs.getString("endereco"));
                cli.setTelefone(rs.getString("telefone"));
                cli.setDataCadastro(rs.getDate("dataCadastro"));
                lista.add(cli);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
        return lista;
    }
    public static ArrayList<Cliente> getAllCaractere(String string) {
        String selectSQL = "select * from cliente where nome LIKE ?";
        ArrayList<Cliente> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            PreparedStatement ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, "%"+string+"%");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Cliente cli = new Cliente(); 
                cli.setCpf(rs.getString("cpfcliente"));
                cli.setNome(rs.getString("nome"));
                cli.setDataNasc(rs.getDate("dataNasc"));
                cli.setEndereco(rs.getString("endereco"));
                cli.setTelefone(rs.getString("telefone"));
                cli.setDataCadastro(rs.getDate("dataCadastro"));
                lista.add(cli);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
        return lista;
    }
    
    public static ArrayList<Cliente> getAllCPF(String string) {
        String selectSQL = "select * from cliente where cpfcliente LIKE ?";
        ArrayList<Cliente> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            PreparedStatement ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, string+"%");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Cliente cli = new Cliente(); 
                cli.setCpf(rs.getString("cpfcliente"));
                cli.setNome(rs.getString("nome"));
                cli.setDataNasc(rs.getDate("dataNasc"));
                cli.setEndereco(rs.getString("endereco"));
                cli.setTelefone(rs.getString("telefone"));
                cli.setDataCadastro(rs.getDate("dataCadastro"));
                lista.add(cli);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
        return lista;
    }
}
